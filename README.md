# guanbo-docs

Documentation for the guanbo project, built with [mdBook](https://github.com/rust-lang/mdBook) and hosted at [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
