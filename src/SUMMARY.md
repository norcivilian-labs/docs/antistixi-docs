# Summary

- [Getting Started](./getting_started.md)
- [Tutorial](./tutorial.md)
- [User Guides](./user_guides.md)
  - [Playing song chords](./01_playing_song_chords.md)
- [Design](./design.md)
- [Requirements](./requirements.md)
