# Getting Started

We write music together in programming languages.

To listen to the music, open our [sound server](https://broadcast.norcivilianlabs.org).

Next, learn how to play a simple loop in the [Tutorial](./tutorial.md).
