# Requirements

# aerobic sing make
listener must hear public audio stream
# decrease soon front 
listener must see public video stream
# reform act trade
composer must send livecoding loops
# shuffle grab squeeze
composer must stop loop
# remove outer country
composer must hear audio stream
# beyond luggage motor
composer can send to authorized stream
# lend engage develop
composer must see livecoding logs
# daughter glance accident
arranger must add instruments
# survey strategy resist
arranger must remove instruments
# once cook also
arranger can deploy entire infrastructure as code
# wine sand orphan
listener must donate wealth
# bridge canoe exercise
composer must receive wealth
# anger alpha bitter
arranger must redistribute wealth
# onion taxi erode 
composer must write code in any language
# agent setup lamp
listener must see code for the music
# universe flower subject
listener can see music theory analysis of sounds
# consider file peace
listener can become composer with ease
# still mosquito scale
listener can become arranger with ease
# absurd huge one
listener must know that the music is algorithmic
# light crazy solar
composer can copy music of other composers
# alien march quarter
listener won't be intimidated by the source of musical pieces
# grape find industry 
listener must experience memorable music
# rack remind allow 
composer must publish code for the musical pieces
# insane supreme rack 
composer must specify a creative commons license for musical pieces
# brand citizen jungle 
composer must see version-controlled history of development of musical ideas
# famous abuse remember 
composer must implement music theory
# bounce pause garage 
arranger must reads midi from osc 
# ivory decline tonight 
arranger must synthesize sounds with samplers, instruments, lv2 plugins
# glance bachelor hub
arranger can choose musical piece from list
# march wreck program 
arranger won't dummy virtual midi device
# latin sustain vacuum 
arranger must convert OSC messages to midi using o2m
# awesome use gravity 
arranger can mix several musical pieces
# grid you tissue 
arranger must broadcast resulting midi from the mix to OSC over public network using m2o
# never dirt color 
arranger must convert midi messages to sounds
# fortune mirror better 
arranger can perform musical pieces alongside the orchestrator
# disorder retire verify 
arranger can approve a set of composers to send real-time OSC messages
# hair dismiss swamp
composer can send OSC messages in real-time to the orchestrator if approved by the arranger
# mask biology educate 
arranger can write the orchestrator in liquidsoap with lv2 plugins
# vivid cable sun 
arranger can assemble a plugin graph for the orchestrator in an audio plugin host
# fashion miss cotton 
arranger must resolve a set of musical pieces and orchestrations to a constant grid
# shrimp camp illegal 
arranger must stream audio over the public network
# settle oppose sustain 
arranger can broadcast with icecast
# fragile pass end 
arranger can join the broadcast with butt
# hover disagree normal 
arranger can mix another broadcast
# puppy opinion disorder
listener can choose broadcast from list at radio-browser.info
# corn normal rib 
listener can read what musical piece is playing
# rapid inquiry add 
listener can read code of musical piece
# large end type 
composer must must return a stream of MIDI messages
# mansion advance distance 
composer must test the musical piece's midi messages on a synthesizer
# angry ethics execute 
composer must execute the musical piece and return a stream of OSC messages encapsulating MIDI in osmid format
# track decline fresh 
composer can write musical piece in any programming language that implements performer
